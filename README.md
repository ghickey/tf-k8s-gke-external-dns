# External-DNS Terraform Module

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=0.12 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_null"></a> [null](#provider\_null) | n/a |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_gke-iam"></a> [gke-iam](#module\_gke-iam) | git::ssh://git@gitlab.com/ghickey/tf-gke-iam.git | v0.9.1 |

## Resources

| Name | Type |
|------|------|
| [helm_release.external_dns](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [null_resource.deployment_patch](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.helm_repo_setup](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [terraform_remote_state.cluster](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_annotation"></a> [annotation](#input\_annotation) | n/a | `any` | `null` | no |
| <a name="input_chart_name"></a> [chart\_name](#input\_chart\_name) | Chart to install | `string` | `"bitnami/external-dns"` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Chart version to install | `string` | `"6.7.4"` | no |
| <a name="input_cluster_state_file"></a> [cluster\_state\_file](#input\_cluster\_state\_file) | Location of the cluster TF state file | `string` | `""` | no |
| <a name="input_department"></a> [department](#input\_department) | Department ID to which this resource belongs | `any` | n/a | yes |
| <a name="input_dns_provider"></a> [dns\_provider](#input\_dns\_provider) | n/a | `any` | `null` | no |
| <a name="input_env_repo"></a> [env\_repo](#input\_env\_repo) | The Terragrunt or environment repo that this module was called from. | `any` | n/a | yes |
| <a name="input_environment_name"></a> [environment\_name](#input\_environment\_name) | The environment (dev, test, prod) | `any` | n/a | yes |
| <a name="input_kube_config_path"></a> [kube\_config\_path](#input\_kube\_config\_path) | Path to kube config for authenticating to Kubernetes | `string` | `"~/.kube/config"` | no |
| <a name="input_log_level"></a> [log\_level](#input\_log\_level) | n/a | `string` | `"info"` | no |
| <a name="input_metrics_enabled"></a> [metrics\_enabled](#input\_metrics\_enabled) | Enable Prometheus metrics endpoint | `string` | `"true"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | The namespace to deploy Tiller into. | `string` | `"kube-system"` | no |
| <a name="input_nugget"></a> [nugget](#input\_nugget) | Naming moniker for relating all deployed objects | `any` | `null` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | The overall product that will be deployed in this infrastructure | `any` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Google project ID | `any` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The region to deploy to (e.g. us-east-1) | `any` | n/a | yes |
| <a name="input_release_name"></a> [release\_name](#input\_release\_name) | The release name that Helm knows this resource by | `string` | `"external-dns"` | no |
| <a name="input_replicas"></a> [replicas](#input\_replicas) | Number of replicas to create | `string` | `"1"` | no |
| <a name="input_state_bucket"></a> [state\_bucket](#input\_state\_bucket) | The bucket name for terraform state | `any` | `null` | no |
| <a name="input_txt_owner_id"></a> [txt\_owner\_id](#input\_txt\_owner\_id) | TXT record identifier for this installation of external-dns | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_chart_metadata"></a> [chart\_metadata](#output\_chart\_metadata) | Metadata collected from chart deployment |
