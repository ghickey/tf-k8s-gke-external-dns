
terraform {
  required_version = ">=0.12"

  backend "http" {}
}

data "terraform_remote_state" "cluster" {
  backend = "http"
  config = {
    address = var.cluster_state_file
  }
}

# provider "kubernetes" {
#   load_config_file = false

#   host = "https://${data.terraform_remote_state.cluster.outputs.endpoint}"
#   cluster_ca_certificate = base64decode(data.terraform_remote_state.cluster.outputs.cluster_ca_certificate)

#   token = var.k8s_token
# }

provider "helm" {
  kubernetes {
    config_path = var.kube_config_path
    # host                   = data.terraform_remote_state.cluster.outputs.endpoint
    # token                  = var.k8s_token
    # #client_certificate     = base64decode(data.terraform_remote_state.cluster.outputs.client_certificate)
    # #client_key             = base64decode(data.terraform_remote_state.cluster.outputs.client_key)
    # cluster_ca_certificate = base64decode(data.terraform_remote_state.cluster.outputs.cluster_ca_certificate)
  }
}

resource "null_resource" "helm_repo_setup" {
  provisioner "local-exec" {
    command = "helm repo add bitnami https://charts.bitnami.com/bitnami"
  }
}

module "gke-iam" {
  source = "git::ssh://git@gitlab.com/ghickey/tf-gke-iam.git?ref=v0.9.1"
  #source = "/Users/hickey/gitlab/terraform/tf-gke-iam"

  name = "${data.terraform_remote_state.cluster.outputs.name}-external-dns"
  k8s_sa = "external-dns"
  project = var.project
  principals = [
    {
      name = "ChangeResourceRecordSets"
      list = [ "route53:ChangeResourceRecordSets" ]
      resource = [ "*" ]
    },
    {
      name = "ListResourceRecordSets"
      list = [ "route53:List*" ]
      resource = [ "*" ]
    }
  ]

  depends_on = [
    null_resource.helm_repo_setup
  ]
}

locals {
  gke_annotation = var.dns_provider == "google" ? "iam.gke.io/gcp-service-account: ${module.gke-iam.email}" : ""
  patch_json = "{\"spec\": {\"template\": {\"spec\": {\"nodeSelector\": {\"iam.gke.io/gke-metadata-server-enabled\": \"true\"}}}}}"
}

resource "helm_release" "external_dns" {
  name         = var.release_name
  chart        = var.chart_name
  version      = var.chart_version
  namespace    = var.namespace
  force_update = true

  values = [ <<EOF
provider: ${var.dns_provider}
google:
  project: ${var.project}
rbac:
  create: true
serviceAccount:
  annotations:
    ${var.annotation == null ? "" : var.annotation}
    ${local.gke_annotation}
policy: sync
replicas: ${var.replicas}
metrics:
  enabled: ${var.metrics_enabled}
registry: txt
txtOwnerId: ${local.txt_owner_id}
logLevel: debug
EOF
]

}

resource "null_resource" "deployment_patch" {
  provisioner "local-exec" {
    command = "kubectl patch deployment ${helm_release.external_dns.name} --namespace ${var.namespace} --patch '${local.patch_json}'"

  }

  depends_on = [ helm_release.external_dns ]
}