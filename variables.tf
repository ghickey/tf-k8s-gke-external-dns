
variable "namespace" {
  description = "The namespace to deploy Tiller into."
  type        = string
  default     = "kube-system"
}

variable "release_name" {
  description = "The release name that Helm knows this resource by"
  type        = string
  default     = "external-dns"
}

variable "chart_name" {
  description = "Chart to install"
  type        = string
  default     = "bitnami/external-dns"
}

variable "chart_version" {
  description = "Chart version to install"
  type        = string
  default     = "6.7.4"
}

variable "txt_owner_id" {
  description = "TXT record identifier for this installation of external-dns"
  type        = string
  default     = null
}

variable "metrics_enabled" {
  description = "Enable Prometheus metrics endpoint"
  default     = "true"
}

variable "replicas" {
  description = "Number of replicas to create"
  type        = string
  default     = "1"
}

variable "cluster_state_file" {
  description = "Location of the cluster TF state file"
  type        = string
  default     = ""
}

variable "kube_config_path" {
  default     = "~/.kube/config"
  description = "Path to kube config for authenticating to Kubernetes"
}

variable "dns_provider" {
  default     = null
  description = ""
}

variable "annotation" {
  default     = null
  description = ""
}

variable "log_level" {
  default     = "info"
  description = ""
}

locals {
  txt_owner_id = var.txt_owner_id != null ? var.txt_owner_id : "unknown"
}
