
# output "service_account_name" {
#   description = "Name of the service account created for external-dns"
#   value       = google_service_account.external_dns.name
# }

# output "service_account_id" {
#   description = "ID of the created service account"
#   value       = google_service_account.external_dns.id
# }

# output "service_account_email" {
#   description = "Email address of the created service account"
#   value       = google_service_account.external_dns.email
# }

output "chart_metadata" {
  description = "Metadata collected from chart deployment"
  value       = helm_release.external_dns.metadata
}
